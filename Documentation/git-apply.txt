git-apply(1)
============

NAME
----
git-apply - Apply a patch on a git index file and a working tree


SYNOPSIS
--------
[verse]
'git-apply' [--stat] [--numstat] [--summary] [--check] [--index]
	  [--apply] [--no-add] [--index-info] [-R | --reverse]
	  [--allow-binary-replacement | --binary] [--reject] [-z]
	  [-pNUM] [-CNUM] [--inaccurate-eof] [--cached]
	  [--whitespace=<nowarn|warn|error|error-all|strip>]
	  [--exclude=PATH] [--verbose] [<patch>...]

DESCRIPTION
-----------
Reads supplied diff output and applies it on a git index file
and a work tree.

OPTIONS
-------
<patch>...::
	The files to read patch from.  '-' can be used to read
	from the standard input.

--stat::
	Instead of applying the patch, output diffstat for the
	input.  Turns off "apply".

--numstat::
	Similar to \--stat, but shows number of added and
	deleted lines in decimal notation and pathname without
	abbreviation, to make it more machine friendly.  For
	binary files, outputs two `-` instead of saying
	`0 0`.  Turns off "apply".

--summary::
	Instead of applying the patch, output a condensed
	summary of information obtained from git diff extended
	headers, such as creations, renames and mode changes.
	Turns off "apply".

--check::
	Instead of applying the patch, see if the patch is
	applicable to the current work tree and/or the index
	file and detects errors.  Turns off "apply".

--index::
	When --check is in effect, or when applying the patch
	(which is the default when none of the options that
	disables it is in effect), make sure the patch is
	applicable to what the current index file records.  If
	the file to be patched in the work tree is not
	up-to-date, it is flagged as an error.  This flag also
	causes the index file to be updated.

--cached::
	Apply a patch without touching the working tree. Instead, take the
	cached data, apply the patch, and store the result in the index,
	without using the working tree. This implies '--index'.

--index-info::
	Newer git-diff output has embedded 'index information'
	for each blob to help identify the original version that
	the patch applies to.  When this flag is given, and if
	the original version of the blob is available locally,
	outputs information about them to the standard output.

-R, --reverse::
	Apply the patch in reverse.

--reject::
	For atomicity, gitlink:git-apply[1] by default fails the whole patch and
	does not touch the working tree when some of the hunks
	do not apply.  This option makes it apply
	the parts of the patch that are applicable, and leave the
	rejected hunks in corresponding *.rej files.

-z::
	When showing the index information, do not munge paths,
	but use NUL terminated machine readable format.  Without
	this flag, the pathnames output will have TAB, LF, and
	backslash characters replaced with `\t`, `\n`, and `\\`,
	respectively.

-p<n>::
	Remove <n> leading slashes from traditional diff paths. The
	default is 1.

-C<n>::
	Ensure at least <n> lines of surrounding context match before
	and after each change.  When fewer lines of surrounding
	context exist they all must match.  By default no context is
	ever ignored.

--unidiff-zero::
	By default, gitlink:git-apply[1] expects that the patch being
	applied is a unified diff with at least one line of context.
	This provides good safety measures, but breaks down when
	applying a diff generated with --unified=0. To bypass these
	checks use '--unidiff-zero'.
+
Note, for the reasons stated above usage of context-free patches are
discouraged.

--apply::
	If you use any of the options marked "Turns off
	'apply'" above, gitlink:git-apply[1] reads and outputs the
	information you asked without actually applying the
	patch.  Give this flag after those flags to also apply
	the patch.

--no-add::
	When applying a patch, ignore additions made by the
	patch.  This can be used to extract common part between
	two files by first running `diff` on them and applying
	the result with this option, which would apply the
	deletion part but not addition part.

--allow-binary-replacement, --binary::
	Historically we did not allow binary patch applied
	without an explicit permission from the user, and this
	flag was the way to do so.  Currently we always allow binary
	patch application, so this is a no-op.

--exclude=<path-pattern>::
	Don't apply changes to files matching the given path pattern. This can
	be useful when importing patchsets, where you want to exclude certain
	files or directories.

--whitespace=<option>::
	When applying a patch, detect a new or modified line
	that ends with trailing whitespaces (this includes a
	line that solely consists of whitespaces).  By default,
	the command outputs warning messages and applies the
	patch.
	When gitlink:git-apply[1] is used for statistics and not applying a
	patch, it defaults to `nowarn`.
	You can use different `<option>` to control this
	behavior:
+
* `nowarn` turns off the trailing whitespace warning.
* `warn` outputs warnings for a few such errors, but applies the
  patch (default).
* `error` outputs warnings for a few such errors, and refuses
  to apply the patch.
* `error-all` is similar to `error` but shows all errors.
* `strip` outputs warnings for a few such errors, strips out the
  trailing whitespaces and applies the patch.

--inaccurate-eof::
	Under certain circumstances, some versions of diff do not correctly
	detect a missing new-line at the end of the file. As a result, patches
	created by such diff programs do not record incomplete lines
	correctly. This option adds support for applying such patches by
	working around this bug.

-v, --verbose::
	Report progress to stderr. By default, only a message about the
	current patch being applied will be printed. This option will cause
	additional information to be reported.

Configuration
-------------

apply.whitespace::
	When no `--whitespace` flag is given from the command
	line, this configuration item is used as the default.


Author
------
Written by Linus Torvalds <torvalds@osdl.org>

Documentation
--------------
Documentation by Junio C Hamano

GIT
---
Part of the gitlink:git[7] suite
