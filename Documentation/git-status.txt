git-status(1)
=============

NAME
----
git-status - Show the working tree status


SYNOPSIS
--------
'git-status' <options>...

DESCRIPTION
-----------
Examines paths in the working tree that has changes unrecorded
to the index file, and changes between the index file and the
current HEAD commit.  The former paths are what you _could_
commit by running 'git add' before running 'git
commit', and the latter paths are what you _would_ commit by
running 'git commit'.

If there is no path that is different between the index file and
the current HEAD commit, the command exits with non-zero
status.

The command takes the same set of options as `git-commit`; it
shows what would be committed if the same options are given to
`git-commit`.


OUTPUT
------
The output from this command is designed to be used as a commit
template comments, and all the output lines are prefixed with '#'.


CONFIGURATION
-------------

The command honors `color.status` (or `status.color` -- they
mean the same thing and the latter is kept for backward
compatibility) and `color.status.<slot>` configuration variables
to colorize its output.

See Also
--------
gitlink:gitignore[5]

Author
------
Written by Linus Torvalds <torvalds@osdl.org> and
Junio C Hamano <junkio@cox.net>.

Documentation
--------------
Documentation by David Greaves, Junio C Hamano and the git-list <git@vger.kernel.org>.

GIT
---
Part of the gitlink:git[7] suite
