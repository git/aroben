git-branch(1)
=============

NAME
----
git-branch - List, create, or delete branches

SYNOPSIS
--------
[verse]
'git-branch' [--color | --no-color] [-r | -a]
	   [-v [--abbrev=<length> | --no-abbrev]]
'git-branch' [--track | --no-track] [-l] [-f] <branchname> [<start-point>]
'git-branch' (-m | -M) [<oldbranch>] <newbranch>
'git-branch' (-d | -D) [-r] <branchname>...

DESCRIPTION
-----------
With no arguments given a list of existing branches
will be shown, the current branch will be highlighted with an asterisk.
Option `-r` causes the remote-tracking branches to be listed,
and option `-a` shows both.

In its second form, a new branch named <branchname> will be created.
It will start out with a head equal to the one given as <start-point>.
If no <start-point> is given, the branch will be created with a head
equal to that of the currently checked out branch.

When a local branch is started off a remote branch, git can setup the
branch so that gitlink:git-pull[1] will appropriately merge from that
remote branch.  If this behavior is desired, it is possible to make it
the default using the global `branch.autosetupmerge` configuration
flag.  Otherwise, it can be chosen per-branch using the `--track`
and `--no-track` options.

With a '-m' or '-M' option, <oldbranch> will be renamed to <newbranch>.
If <oldbranch> had a corresponding reflog, it is renamed to match
<newbranch>, and a reflog entry is created to remember the branch
renaming. If <newbranch> exists, -M must be used to force the rename
to happen.

With a `-d` or `-D` option, `<branchname>` will be deleted.  You may
specify more than one branch for deletion.  If the branch currently
has a ref log then the ref log will also be deleted. Use -r together with -d
to delete remote-tracking branches.


OPTIONS
-------
-d::
	Delete a branch. The branch must be fully merged.

-D::
	Delete a branch irrespective of its index status.

-l::
	Create the branch's ref log.  This activates recording of
	all changes to made the branch ref, enabling use of date
	based sha1 expressions such as "<branchname>@{yesterday}".

-f::
	Force the creation of a new branch even if it means deleting
	a branch that already exists with the same name.

-m::
	Move/rename a branch and the corresponding reflog.

-M::
	Move/rename a branch even if the new branchname already exists.

--color::
	Color branches to highlight current, local, and remote branches.

--no-color::
	Turn off branch colors, even when the configuration file gives the
	default to color output.

-r::
	List or delete (if used with -d) the remote-tracking branches.

-a::
	List both remote-tracking branches and local branches.

-v::
	Show sha1 and commit subject line for each head.

--abbrev=<length>::
	Alter minimum display length for sha1 in output listing,
	default value is 7.

--no-abbrev::
	Display the full sha1s in output listing rather than abbreviating them.

<branchname>::
	The name of the branch to create or delete.
	The new branch name must pass all checks defined by
	gitlink:git-check-ref-format[1].  Some of these checks
	may restrict the characters allowed in a branch name.

<start-point>::
	The new branch will be created with a HEAD equal to this.  It may
	be given as a branch name, a commit-id, or a tag.  If this option
	is omitted, the current branch is assumed.

<oldbranch>::
	The name of an existing branch to rename.

<newbranch>::
	The new name for an existing branch. The same restrictions as for
	<branchname> applies.


Examples
--------

Start development off of a known tag::
+
------------
$ git clone git://git.kernel.org/pub/scm/.../linux-2.6 my2.6
$ cd my2.6
$ git branch my2.6.14 v2.6.14   <1>
$ git checkout my2.6.14
------------
+
<1> This step and the next one could be combined into a single step with
"checkout -b my2.6.14 v2.6.14".

Delete unneeded branch::
+
------------
$ git clone git://git.kernel.org/.../git.git my.git
$ cd my.git
$ git branch -d -r todo html man   <1>
$ git branch -D test               <2>
------------
+
<1> delete remote-tracking branches "todo", "html", "man"
<2> delete "test" branch even if the "master" branch does not have all
commits from test branch.


Notes
-----

If you are creating a branch that you want to immediately checkout, it's
easier to use the git checkout command with its `-b` option to create
a branch and check it out with a single command.


Author
------
Written by Linus Torvalds <torvalds@osdl.org> and Junio C Hamano <junkio@cox.net>

Documentation
--------------
Documentation by Junio C Hamano and the git-list <git@vger.kernel.org>.

GIT
---
Part of the gitlink:git[7] suite
